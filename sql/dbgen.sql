DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS blogs;
DROP TABLE IF EXISTS permission;
DROP TABLE IF EXISTS account;

CREATE TABLE account (
 user_id MEDIUMINT NOT NULL AUTO_INCREMENT,
 username varchar(64) NOT NULL,
 fname varchar(20) NOT NULL,
 lname varchar(20) NOT NULL,
 birthday date NOT NULL,
 description varchar(100) NOT NULL,
 encrypt_pwd CHAR(60),
 avatar VARCHAR(64) NOT NULL,
 PRIMARY KEY (user_id)
);

CREATE TABLE permission (
    per_id MEDIUMINT NOT NULL ,
    permission_level BOOLEAN,
    token VARCHAR(60),
    PRIMARY KEY (per_id),
    FOREIGN KEY (per_id) references account(user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE blogs (
    blog_id MEDIUMINT AUTO_INCREMENT,
    user_id MEDIUMINT,
    post_title tinytext,
--     add tag 记得要重新创建blog哦~
    post_tag1 VARCHAR(32) NOT NULL,
    post_tag2 VARCHAR(32),
    post_tag3 VARCHAR(32),
    -- delete fname,lname, since we only use "@user_id" as user identifier.
    post_score int,
    post_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    post_content varchar (300),
    post_image tinytext,
    post_video tinytext,
    edit boolean,
    PRIMARY KEY (blog_id),
    FOREIGN KEY (user_id) references account (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- add tags table 记得要在数据库创建哦~
CREATE TABLE tags (
  tag_id MEDIUMINT AUTO_INCREMENT,
  tag_content VARCHAR(20),
  PRIMARY KEY (tag_id)
);

CREATE TABLE comment (
    comment_id MEDIUMINT AUTO_INCREMENT,
    blog_id MEDIUMINT,
    user_id MEDIUMINT,
    post_date timestamp,
    parent_comment_id MEDIUMINT,
    visible boolean,
    comment varchar(140),
    PRIMARY KEY (comment_id),
--     FOREIGN KEY (parent_comment_id) references comment (comment_id),
    FOREIGN KEY (user_id) references account (user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (blog_id) references blogs (blog_id) ON DELETE CASCADE ON UPDATE CASCADE
);


# insert into account (user_id, username, fname, lname, birthday, description, encrypt_pwd, avatar) VALUES
# (1,'yan1','YAN1','LIU','19800101','yan1.liu','$2b$10$4UeBJcjI2jT17FyTIqFzGexSglVoNoulPTxBkEH/meBqWLf0B4Edu','1.jpg'),
# (2,'yan2','yan2','LIU','19800102','yan2.liu','$2b$10$X6sgN4yHUGyIWC9QaLQPTuSYamloWT.ZxOm1tfZHU1nhdNYv5xW0q','2.jpg'),
# (3,'yan3','YAN3','LIU','19800103','yan3.liu','$2b$10$kSEFWMIxdS2j9gNyD8f69ebmoZX.GIzPHPE/5IBZ/ZZPSgdHiNNku','3.jpg'),
# (4,'yan4','YAN4','LIU','19800104','yan4.liu','$2b$10$O6p51QmQVv8FD2SbtRTgju88Ei/fXxt7UK.XBqffZPJSVFFW/8iMK','4.jpg'),
# (5,'yan5','YAN5','LIU','19800105','yan5.liu','$2b$10$7lm76M3GejxYDbTr8nUtpObNprdwkO1CLy7AOgeEBZdL8B/4VIvOe','5.jpg'),
# (6,'yan6','YAN6','LIU','19800106','YAN6.LIU','$2b$10$md.1VV0UZXd92q7ene0UWOhkGrTpBi1gjU6.41Lbww8hRPuE3m79e','6.jpg');

# insert into blogs (blog_id, user_id, post_title, post_tag1, post_tag2, post_tag3, post_score, post_date, post_content) values
# (1, 1, 'COVID-19.LIVE','love','instagood','cute', 303,20210528,'New York: COVID-19: News and updates for New York \n,  Syringe have been tested in large, randomized controlled trials that include people of');
# insert into blogs (blog_id, user_id, post_title, post_tag1, post_tag2, post_tag3,  post_score, post_date, post_content) values
# (2, 2, 'Dillon Brooks','love','instagood','cute',  123,20210519,'Memphis forward Dillon Brooks leads the Grizzlies to a 100-96 victory over the Spurs with 24 points, seven rebounds and two steals');
# insert into blogs (blog_id, user_id, post_title, post_tag1, post_tag2, post_tag3, post_score, post_date, post_image) values
# (3, 3, 'Lorem ipsum dolor sit amet ~ !!','love','instagood','cute', 188,20210523,  '3.jpg');
# insert into blogs (blog_id, user_id, post_title, post_tag1, post_tag2, post_tag3,  post_score, post_date, post_image) values
# (4, 3, 'Exclusive Q&A: Matthew Stafford on the Lions, the Rams and his old friend Clayton Kershaw','love','instagood','cute', 215,20210510, '4.jpg');
# insert into blogs (blog_id, user_id, post_title, post_tag1, post_tag2, post_tag3,  post_score, post_date, post_image) values
# (5, 1,'Try to post a video, wow ','fashion','bored','all_shots',152,20210518,'1622380490842_7.mp4');
# insert into blogs (blog_id, user_id, post_title,  post_tag1, post_tag2, post_tag3, post_score, post_date, post_content) values
# (6, 4, 'Meet the ''army of 16-year-olds'' that are taking on the Democrats','love','instagood','cute', 189,20210516,'Young progressives are emerging as a political force in Massachusetts elections, and taking aim at the Democratic establishment.Photo via @nytimes');


insert into permission (per_id, permission_level) VALUES -- true means that this user is admin
(1, TRUE),
(2, TRUE),
(3, TRUE),
(4, FALSE),
(5, FALSE),
(6, FALSE);

-- add some tags value 记得要在数据库加进去哦~
insert into tags values
(1, 'love'),
(2, 'instagood'),
(3, 'me'),
(4, 'cute'),
(5, 'happy'),
(6, 'friends'),
(7, 'fun'),
(8, 'fashion'),
(9, 'food'),
(10, 'bored'),
(11, 'all_shots'),
(12, 'fitness'),
(13, 'COVID'),
(14, 'Venom'),
(15, 'Loki'),
(16, 'family'),
(17, 'fashion_week'),
(18, 'fast&furious9'),
(19, 'fashion'),
(20, 'fashion'),
(21, 'fashion'),
(22, 'fun'),
(23, 'fun'),
(24, 'food'),
(25, 'food'),
(26, 'food'),
(27, 'food'),
(28, 'fast&furious9'),
(29, 'fast&furious9'),
(30, 'fast&furious9'),
(31, 'fast&furious9'),
(32, 'fast&furious9'),
(33, 'fast&furious9'),
(34, 'fast&furious9'),
(35, 'COVID'),
(36, 'COVID'),
(37, 'COVID'),
(38, 'COVID'),
(39, 'COVID'),
(40, 'Venom'),
(41, 'Loki'),
(42, 'Venom'),
(43, 'Loki'),
(44, 'Venom'),
(45, 'Loki'),
(46, 'Venom'),
(47, 'Loki'),
(48, 'Doom'),
(49, 'Doom'),
(50, 'Health'),
(51, 'Zoom'),
(52, 'Space-X'),
(53, 'Java'),
(54, 'C#'),
(55, 'Python'),
(56, 'iPad'),
(57, 'iMac'),
(58, 'iCloud'),
(59, 'iTunes'),
(60, 'Apple'),
(61, 'Apple'),
(62, 'Apple'),
(63, 'Book'),
(64, 'Waikato'),
(65, 'NewZealand'),
(66, 'Hamilton'),
(67, 'Auckland'),
(68, 'Christchurch'),
(69, 'Otago'),
(70, 'Wanka'),
(71, 'Wellington'),
(72, 'Queenstown'),
(73, 'NewZealand'),
(74, 'Dunedin'),
(76, 'Sky Tower'),
(77, 'Piha Beach'),
(78, 'Piha Beach');


# insert into comment (comment_id,blog_id, user_id, post_date, parent_comment_id,visible, comment) VALUES
# (1, 1, 2, '2021-05-21 20:08:08', NULL, true, 'Lorem ipsum dolor sit amet ~ !! turpis at eros facilisis mollis. Aliquam '),
# (2, 1, 3, '2021-05-23 20:08:08', 1, true, 'Couldn''t agree more.'),
# (3, 1, 4, '2021-05-19 20:08:08', NULL,true,  'turpis ~~%$ LOL~~ at eros facilisis mollis'),
# (4, 1, 1, '2021-05-19 20:08:08', 2, true, 'I don''t think so...'),
# (5, 1, 4, '2021-05-09 20:08:08', 3,true,  '   ^_^'),
# (6, 1, 1, '2021-05-08 20:08:08', 1, true, '   -:)'),
# (7, 1, 3, '2021-05-19 20:08:08', NULL, true, ' facilisis mollis ……&*（0'),
# (8, 1, 4, '2021-05-19 20:08:08', 7,true,  '!! turpis at eros'),
# (9, 1, 2, '2021-05-09 20:08:08', 8,true,  ' dolor sit amet ~ ^_^'),
# (10, 1, 2, '2021-05-08 20:08:08', 8,true,  'O M G.....'),
# (11, 1, 3, '2021-05-19 20:08:08', NULL,true,  ' facilisis mollis ……&*（0'),
# (12, 1, 1, '2021-05-19 20:08:08', 11,true,  '!! turpis at eros'),
# (13, 1, 3, '2021-05-09 20:08:08',11, true, ' dolor sit amet ~ ^_^'),
# (14, 1, 1, '2021-05-08 20:08:08', 11, true, 'O M G.....'),
# (15, 2, 2, '2021-05-21 20:08:08', NULL,true, 'Lorem ipsum dolor sit amet ~ !! turpis at eros facilisis mollis. Aliquam '),
# (16, 2, 3, '2021-05-23 20:08:08', 15, true, 'Couldn''t agree more.'),
# (17, 2, 4, '2021-05-19 20:08:08', NULL, true, 'turpis ~~%$ LOL~~ at eros facilisis mollis'),
# (18, 2, 1, '2021-05-19 20:08:08', 16, true, 'I don''t think so...'),
# (19, 3, 2, '2021-05-21 20:08:08', NULL,true, 'Lorem ipsum dolor sit amet ~ !! turpis at eros facilisis mollis. Aliquam '),
# (20, 3, 3, '2021-05-23 20:08:08', 19, true, 'Couldn''t agree more.'),
# (21, 3, 4, '2021-05-19 20:08:08', NULL, true, 'turpis ~~%$ LOL~~ at eros facilisis mollis'),
# (22, 3, 1, '2021-05-19 20:08:08', 20, true, 'I don''t think so...'),
# (23, 4, 2, '2021-05-21 20:08:08', NULL,true, 'Lorem ipsum dolor sit amet ~ !! turpis at eros facilisis mollis. Aliquam '),
# (24, 4, 3, '2021-05-23 20:08:08', 23, true, 'Couldn''t agree more.'),
# (25, 4, 4, '2021-05-19 20:08:08', NULL, true, 'turpis ~~%$ LOL~~ at eros facilisis mollis'),
# (26, 4, 1, '2021-05-19 20:08:08', 24, true, 'I don''t think so...');
