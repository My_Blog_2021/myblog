// commenter delete and author hide routes
const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-zixuan.js");

router.post("/default/isAuthor", async function(req, res) {

    if (req.session.user)  {
        const blogId = req.body.blog_id;
        const blogInfo = await userDao.retrieveBlogByBlogID(blogId);

        // check if is author
        const authorId = blogInfo.user_id;
        const userId = req.session.user.user_id;
        if (authorId === userId) {
            // is author
            res.end("1");
        } else {
            res.end("0");
        }
    } else {
        res.end("0");
    }


});

router.post("/default/hide", async function(req, res) {

        const comment_id = req.body.comment_id;
        let visible = req.body.visible;
        if (visible === "true") {
            visible = true;
        } else {
            visible = false;
        }

        // update comment status
        await userDao.updateCommentStatus(visible, comment_id);
        if (visible) {
            const commentInfo = await userDao.retrieveCommentByCommentID(comment_id);
            const commentContent = commentInfo.comment;
            res.send(commentContent);
        }

});

router.post("/default/isCommenter", async function(req, res) {

    if (req.session.user) {
        const commentId = req.body.comment_id;
        const commentInfo = await userDao.retrieveCommentByCommentID(commentId);

        // check if is commenter
        const commenterId = commentInfo.user_id;
        const userId = req.session.user.user_id;
        if (commenterId === userId) {
            // is commenter
            res.end("1");
        } else {
            res.end("0");
        }
    } else {
        res.end("0");
    }
});
router.post("/default/delete", async function(req, res) {

    const commentId = req.body.comment_id;
    const hasReply = req.body.hasReply;

    if (hasReply === "true") {
        // update comment's content "-- Deleted by comment author! --"
        const updateContent = "-- Deleted by comment author! --";
        await userDao.updateCommentContentById(updateContent, commentId);
    } else {
        // delete comment
        await userDao.deleteCommentById(commentId);
    }


});


module.exports = router;
