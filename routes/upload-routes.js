// Setup Express
const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-kao.js");
const userDao1 = require("../modules/users-dao-zixuan.js");
// Setup fs
const fs = require("fs");
// Setup jimp
const jimp = require("jimp");
// Setup body-parser
const bodyParser = require("body-parser");
router.use(bodyParser.urlencoded({extended: false}));

// Setup multer (files will temporarily be saved in the "temp" folder).
const path = require("path");
const multer = require("multer");
const upload = multer({
    dest: path.join(__dirname, "temp")
});

// Make the "public" folder available statically
router.use(express.static(path.join(__dirname, "public")));


// When we POST to /uploadImage, accept the image upload, and move it to the images folder.
// Then, create a thumbnail for it using jimp.
router.post("/uploadImage", upload.single("imageFile"), async function (req, res) {

    const fileInfo = req.file;
    const author = req.session.user.user_id;
    const title = req.body.title;
    const tag1 = req.body.tag1;
    const tag2= req.body.tag2;
    const tag3 = req.body.tag3;

    // Move the image into the images folder
    const oldFileName = fileInfo.path;
    const name = `${Date.now()}_${fileInfo.originalname}`;
    const newFileName = `./public/uploadedFiles/${name}`;
    fs.renameSync(oldFileName, newFileName);

    //Create and save thumbnail
    const image = await jimp.read(newFileName);
    image.resize(550, jimp.AUTO);
    await image.write(`./public/uploadedFiles/thumbnails/${name}`)

    // insert tags to the tag table------
    await userDao1.createTag(tag1);
    console.log(tag2);
    if (tag2 !== "") {
        await userDao1.createTag(tag2);
    }
    if (tag3 !== "") {
        await userDao1.createTag(tag3);
    }
    //------------------------------------

    let picture = {
        author: author,
        score: 1,
        title: title,
        tag1: tag1,
        tag2: tag2,
        tag3: tag3,
        image: name

    };
    await userDao.uploadPic(picture);
    res.redirect('./home?message=Successfully Created!')
});

router.post("/uploadVideo", upload.single("videoFile"), async function (req, res) {

    const fileInfo = req.file;
    const author = req.session.user.user_id;
    const title = req.body.title;
    const tag1 = req.body.tag1;
    const tag2= req.body.tag2;
    const tag3 = req.body.tag3;

    // Move the video into the folder
    const oldFileName = fileInfo.path;
    const name = `${Date.now()}_${fileInfo.originalname}`;
    const newFileName = `./public/uploadedFiles/${name}`;

    fs.renameSync(oldFileName, newFileName);

    // insert tags to the tag table------
    await userDao1.createTag(tag1);
    console.log(tag2);
    if (tag2 !== "") {
        await userDao1.createTag(tag2);
    }
    if (tag3 !== "") {
        await userDao1.createTag(tag3);
    }
    //------------------------------------

    let video = {
        author: author,
        score: 1,
        title: title,
        tag1: tag1,
        tag2: tag2,
        tag3: tag3,
        video: name

    };
    await userDao.uploadVideo(video);
    res.redirect('./home?message=Successfully Created!');
});

module.exports = router
