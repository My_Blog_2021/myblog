// posts edit and delete routes

const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-zixuan.js");
// Setup fs
const fs = require("fs");
// Setup jimp
const jimp = require("jimp");
// Setup body-parser
const bodyParser = require("body-parser");
router.use(bodyParser.urlencoded({extended: false}));

// Setup multer (files will temporarily be saved in the "temp" folder).
const path = require("path");
const multer = require("multer");
const upload = multer({
    dest: path.join(__dirname, "temp")
});

// Make the "public" folder available statically
router.use(express.static(path.join(__dirname, "public")));


//edit
// check edit type
router.get('/editType', async function (req, res) {
    const blogId = req.query.blogId;
    const blogInfo = await userDao.retrieveBlogByBlogID(blogId);

    res.locals.blogInfo = blogInfo;
    if (blogInfo.post_content != null) {
        res.render('posts-edit', {is_text:true, is_image:false, is_video:false});
    } else if (blogInfo.post_image != null) {
        res.render('posts-edit', {is_text:false, is_image:true, is_video:false});
    } else {
        res.render('posts-edit', {is_text:false, is_image:false, is_video:true});
    }

});

router.post("/editPost", async function (req, res) {
    const blogId = req.body.blogId;
    const title = req.body.title;
    const tag1 = req.body.tag1;
    const tag2= req.body.tag2;
    const tag3 = req.body.tag3;
    const content = req.body.content;

    let post = {
        blog_id: blogId,
        post_title: title,
        post_tag1: tag1,
        post_tag2: tag2,
        post_tag3: tag3,
        post_content: content

    }

    await userDao.updatePostContent(post);
    const isEdit = true;
    await userDao.updateBlogStatus(isEdit, blogId);
    res.redirect('/home?message=Successfully edited!')

});

router.post("/editImage", upload.single("imageFile"), async function (req, res) {
    const blogId = req.body.blogId;
    const title = req.body.title;
    const tag1 = req.body.tag1;
    const tag2= req.body.tag2;
    const tag3 = req.body.tag3;
    const fileInfo = req.file;

    console.log(fileInfo);
    // Move the image into the images folder
    const oldFileName = fileInfo.path;
    const name = `${Date.now()}_${fileInfo.originalname}`;
    const newFileName = `./public/uploadedFiles/${name}`;
    fs.renameSync(oldFileName, newFileName);

    //Create and save thumbnail
    const image = await jimp.read(newFileName);
    image.resize(550, jimp.AUTO);
    await image.write(`./public/uploadedFiles/thumbnails/${name}`)

    let post = {
        blog_id: blogId,
        post_title: title,
        post_tag1: tag1,
        post_tag2: tag2,
        post_tag3: tag3,
        post_image: name
    }

    await userDao.updatePostImage(post);
    const isEdit = true;
    await userDao.updateBlogStatus(isEdit, blogId);
    res.redirect('/home?message=Successfully edited!')


});

router.post("/editVideo", upload.single("videoFile"),async function (req, res) {

    const blogId = req.body.blogId;
    const title = req.body.title;
    const tag1 = req.body.tag1;
    const tag2= req.body.tag2;
    const tag3 = req.body.tag3;
    const fileInfo = req.file;

    // Move the video into the folder
    const oldFileName = fileInfo.path;
    const name = `${Date.now()}_${fileInfo.originalname}`;
    const newFileName = `./public/uploadedFiles/${name}`;

    fs.renameSync(oldFileName, newFileName);

    let post = {
        blog_id: blogId,
        post_title: title,
        post_tag1: tag1,
        post_tag2: tag2,
        post_tag3: tag3,
        post_video: name
    }

    await userDao.updatePostVideo(post);
    const isEdit = true;
    await userDao.updateBlogStatus(isEdit, blogId);
    res.redirect('/home?message=Successfully edited!')

});

router.post("/deletePost", async function (req, res) {
    const blogId = req.body.blogId;
    await userDao.deletePostById(blogId);
    console.log("delete");
    res.end("1");


});


module.exports = router;
