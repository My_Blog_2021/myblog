// account details routes
const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-zixuan.js");
const bcrypt = require("bcrypt");
const fs = require("fs");


// Whenever we navigate to /userInfo, display user's info.
router.get("/userInfo", async function(req, res) {
    if (req.session.user){

        const username = req.session.user.username;
        const userDetails = await userDao.retrieveUserByUsername(username);

        res.locals.details = userDetails;
        res.locals.loggedIn= true;
        res.locals.username = req.session.user.username;

        const birthday = (userDetails.birthday).toString();

        res.locals.birthday = birthday.substring(0,15);

        res.locals.message = req.query.message;
        res.render("accountDetails");
    }else {
        res.redirect('./login?message=Please log in!');
    }
});

// Whenever we navigate to /edit, display user's previous info to edit.
router.get("/edit", async function(req, res) {
    const userId = req.session.user.user_id;
    const userDetailsOld = await userDao.retrieveUserByID(userId);
    res.locals.details = userDetailsOld;
    res.locals.avatars = fs.readdirSync("./public/avatar");
    res.render("edit-delete");
});

// Whenever we navigate to /edit/password, user can edit password.
router.get("/password", function(req, res) {

    res.render("edit-password");
});

// Whenever we post to /edit/password, update user's info by id.
router.post("/password", async function(req, res) {
    const userId = req.session.user.user_id;
    const password1 = req.body.password1;

    // hash and salt password
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    const password = bcrypt.hashSync(password1, salt);

    //update in the database
    await userDao.updateUserPassword(password, userId);

    res.redirect('./userInfo?message=Password edited successfully!');
});

// Whenever we post to /password/check, check user's original password if match.
router.post("/password/check", async function(req, res) {
    const password = req.body.password;
    const username = req.session.user.username
    const user = await userDao.retrieveUserByUsername(username);

    // check if match
    const isMatch = bcrypt.compareSync(password, user.encrypt_pwd);
    if (isMatch) {
        res.end("1");
    } else {
        res.end("0");
    }
});

// Whenever we post to /edit, update user's info by id.
router.post("/edit", async function (req, res) {
    const userId = req.session.user.user_id;
    function getUser() {
        const username = req.body.username;
        const firstName = req.body.firstName;
        const lastName = req.body.lastName;
        const birthDate = req.body.birthDate;
        const description = req.body.description;
        const avatar = req.body.avatar;

        return {
            username: username,
            firstName: firstName,
            lastName: lastName,
            birthDate: birthDate,
            description: description,
            avatar: avatar,
            user_id: userId
        };
    }

    // update the info
    const user = getUser();
    await userDao.updateUser(user);
    req.session.user = user;

    res.redirect('./userInfo?message=Account edited successfully!');

})

// check username by Ajax
router.post("/edit/username", async function(req, res) {
        const username = req.body.username;
        let user = await userDao.retrieveAllUsername();
        let isExist = user.some(function (value, index, array) {
            return (value.username === username);
        });
        console.log(username);
        if (isExist === true) {
            res.end("1");
        } else {
            res.end("0");
        }
    }

)

// Whenever we post to /delete, delete user's account by id.
router.get("/delete",  async function(req, res) {
    const userId = req.session.user.user_id;
    await userDao.deleteUserById(userId).then(function() {
        req.session.user = null;

        res.redirect('/login?message=Account Deleted successfully!');

    } );

});

module.exports = router;
