//post routes
const express = require('express')
const router = express.Router()
const userDao = require("../modules/users-dao-kao.js");
const userDao1 = require("../modules/users-dao-zixuan.js");
const bodyParser = require("body-parser");
router.use(bodyParser.urlencoded({ extended: false}));

// text post page
router.get('/newPost', async function (req, res) {
    if (req.session.user) {
        if (!req.query.type) {}
        // get hot tags-----------------------
        const result = await userDao1.retrieveTagsSorted();
        let sortedTags = [];
        for (let i = 0; i < 5; i++){
            sortedTags.push(result[i].tag_content);
        }
        res.locals.sortedTags = sortedTags;
        //--------------------------------
        res.render('posts',{is_text:true, is_image:false, is_video:false});
    } else {
        res.redirect('/login?message=Please log in!');
    }



});

router.get('/newImg', async function (req, res) {
    // get hot tags-----------------------
    const result = await userDao1.retrieveTagsSorted();
    let sortedTags = [];
    for (let i = 0; i < 5; i++){
        sortedTags.push(result[i].tag_content);
    }
    res.locals.sortedTags = sortedTags;
    //--------------------------------
    res.render('posts', {is_text:false, is_image:true, is_video:false});
});

router.get('/newVideo', async function (req, res) {
    // get hot tags-----------------------
    const result = await userDao1.retrieveTagsSorted();
    let sortedTags = [];
    for (let i = 0; i < 5; i++){
        sortedTags.push(result[i].tag_content);
    }
    res.locals.sortedTags = sortedTags;
    //--------------------------------
    res.render('posts', {is_text:false, is_image:false, is_video:true});
});


// POST /posts/create
router.post('/newPost', async function (req, res) {

    const author = req.session.user.user_id;
    const title = req.body.title;
    const tag1 = req.body.tag1;
    const tag2 = req.body.tag2;
    const tag3 = req.body.tag3;
    const content = req.body.content;

    // insert tags to the tag table------
    await userDao1.createTag(tag1);

    if (tag2 !== "") {
        await userDao1.createTag(tag2);
    }
    if (tag3 !== "") {
        await userDao1.createTag(tag3);
    }
    //------------------------------------

    let post = {
        author: author,
        score: 1,
        title: title,
        tag1: tag1,
        tag2: tag2,
        tag3: tag3,
        content: content
    };

    await userDao.createPost(post);
    req.session.blogs = post;
    res.redirect('./home?message=Successfully Created!');
});


module.exports = router
