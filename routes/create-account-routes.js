// create account routes

const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-zixuan.js");
const bcrypt = require("bcrypt");
const fs = require("fs");
const jimp = require("jimp");

const path = require("path");
const multer = require("multer");
const upload = multer({
    dest: path.join(__dirname, "temp")
});


// Whenever we navigate to /newAccount, render the create-account view.
router.get("/newAccount", function (req, res) {

    res.locals.avatars = fs.readdirSync("./public/avatar");
    res.locals.message = req.query.message;
    res.render("create-account");
});

// check username by Ajax
router.post("/newAccount/username", async function(req, res) {
        const username = req.body.username;
        let user = await userDao.retrieveAllUsername();
        let isExist = user.some(function (value, index, array) {
            return (value.username === username);
        });
        console.log(username);
        if (isExist) {
            res.end("1");

        } else {
            res.end("0");
        }
}

)

// Whenever we POST to /newAccount, create a new account.
router.post("/newAccount",   async function createUser(req, res) {


    function getUser() {
        const username = req.body.username;
        const password1 = req.body.password1;
        const password2 = req.body.password2;
        const firstName = req.body.firstName;
        const lastName = req.body.lastName;
        const birthDate = req.body.birthDate;
        const description = req.body.description;
        const avatar = req.body.avatar;


        // hash and salt password
        const saltRounds = 10;
        const salt = bcrypt.genSaltSync(saltRounds);
        const password = bcrypt.hashSync(password1, salt);

        return {
            username: username,
            password: password,
            firstName: firstName,
            lastName: lastName,
            birthDate: birthDate,
            description: description,
            avatar: avatar
        };
    }

// store in the database
    const user = getUser();
    await userDao.createUser(user);

    res.redirect('./login?message=Account created successfully!');

});



module.exports = router;
