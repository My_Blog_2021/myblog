// posts' like and dislike routes

const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-zixuan.js");

router.post("/posts/score",  async function(req, res) {


        // get current score
        const blog_id = parseInt(req.body.blog_id);
        const score = parseInt(req.body.score);

        const currentBlog = await userDao.retrieveBlogByBlogID(blog_id);
        let currentScore = currentBlog.post_score;

        // update the new score
        currentScore = parseInt(currentScore) + score;
        await userDao.updateScore(blog_id, currentScore);
        res.end(`${currentScore}`);
});

router.post( "/posts/isLogin",   function(req, res) {
        // check if logged in
        if (req.session.user) {
                res.end("1");
        } else {
                res.end("0");
        }
});

module.exports = router;
