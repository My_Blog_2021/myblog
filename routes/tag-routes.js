// tag's routes

const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-zixuan.js");

// tag
router.post("/posts",   async function(req, res) {

    const tag1 = req.body.tag1;
    await userDao.createTag(tag1);

    const tag2 = req.body.tag2;
    if (tag2 !== "") {
        await userDao.createTag(tag2);
    }
    const tag3 = req.body.tag3;
    if (tag3 !== "") {
        await userDao.createTag(tag3);
    }

});

router.post("/posts/filter",   async function(req, res) {

    let tagContent = req.body.tagContent;
    if (tagContent !== "") {
        tagContent = `${tagContent}%`;
        const result =  await userDao.retrieveFilteredTagsByContent(tagContent);

        let filteredTags = [];
        if (result.length > 5) {
            for (let i = 0; i < 5; i++){
                filteredTags.push(result[i].tag_content);
            }
        } else {
            for (let i = 0; i < result.length; i++){
                filteredTags.push(result[i].tag_content);
            }
        }

        res.send(filteredTags);
    }


});


module.exports = router;
