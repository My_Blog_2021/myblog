
const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
// The DAO that handles CRUD operations for users.
const userDao = require("../modules/users-dao-zixuan.js");
const dao = require("../modules/users-dao-yan.js");

// Whenever we navigate to ANY page, make the "user" session object available to the
// Handlebars engine by adding it to res.locals.
router.use(function (req, res, next) {
    res.locals.user = req.session.user;
    next();
});

// Whenever we navigate to /login, if we're already logged in, redirect to "/".
// Otherwise, render the "login" view, supplying the given "message" query parameter
// to the view engine, if any.

// When navigating to "/", show the homepage.
router.get("/", async function  (req, res) {
    res.locals.default = true;
    res.locals.title = "myBlog";
    res.redirect("./default");
});



router.get("/login", function (req, res) {
    res.locals.login= true;
    res.locals.title = "login";
    if (req.session.user) {
        if (req.session.user.user_id === undefined) {
            res.render("login");
        } else {
            res.redirect("/home");
        }
    }
    else {
        res.locals.message = req.query.message;
        res.render("login");
    }
});

router.post("/login", async function (req, res) {
    res.locals.login = true;
    res.locals.title = "myBlog";
    // Get the username and password submitted in the form
    const username = req.body.username;
    const password = req.body.password;

    // check if username exists
    let userAll = await userDao.retrieveAllUsername();
    let isExist = userAll.some(function (value, index, array) {
        return (value.username === username);
    });

    if (isExist) {
        // Find a matching user in the database
        const user = await userDao.retrieveUserByUsername(username);

        // check if there is a matching user...
        const isMatch = bcrypt.compareSync(password, user.encrypt_pwd);
        if (isMatch) {
            req.session.user = user;
            res.redirect("./home?message=Successfully logged in!");
        } else {
            res.redirect("./login?message=Authentication failed!");
        }
    } else {
        res.redirect("./login?message=Username does not exist!");
    }
});


router.get("/default", async function (req, res) {
    res.locals.message = req.query.message;
        if (req.session.user) {
            res.locals.loggedIn= true;
            res.locals.username = req.session.user.username;
            res.locals.user_id = req.session.user.user_id;
        }
        res.locals.default= true;
        res.locals.title = "default";
    let posts;
    if (req.cookies.posts==undefined){
        posts = await dao.searchAndSortBy("post_date", "", "");
        res.locals.posts = posts;

    } else {
    res.locals.posts =  req.cookies.posts;

    }

    for (let i=0;i<res.locals.posts.length;i++){   // res.locals.posts.length  loop through res.locals.posts, for every of them:
    //console.log("res.locals.posts[i].blog_id  " + res.locals.posts[i].blog_id);

        let comments=[];
        let commentsNull=[];
        let commentsLevel1=[];
        let commentsLevel2=[];
        comments = await dao.getAllCommentsForEachBlog(res.locals.posts[i].blog_id); //get all comments from db

        for (let j=0;j<comments.length;j++){ //loop through comments, for each one:
            if (comments[j].parent_comment_id==null){//if parent_id==null that means it's level 1 comment
                commentsNull.push(comments[j]); }
        }

        res.locals.posts[i].comments=commentsNull;
        res.locals.posts[i].commentsNum = comments.length;

       // console.log("i blog_id comments " + i + " " + res.locals.posts[i].blog_id + " " + res.locals.posts[i].comments);
        //get level 0 comments;
        for (let k=0;k<commentsNull.length;k++) { //update post_date with only 18 chars
            res.locals.posts[i].comments[k].post_date=(commentsNull[k].post_date.toString()).substring(4,21);
        }

        for (let j=0;j<commentsNull.length;j++) {
            commentsLevel1 = await dao.getAllSubComments(res.locals.posts[i].blog_id, commentsNull[j].comment_id);
            res.locals.posts[i].comments[j].commentsLevel1 = commentsLevel1; //get level 1 comments;

            for (let k=0;k<commentsLevel1.length;k++) { //update post_date with only 18 chars
                res.locals.posts[i].comments[j].commentsLevel1[k].post_date=(commentsLevel1[k].post_date.toString()).substring(4,21);
            }

            for (let l=0;l<commentsLevel1.length;l++){
                commentsLevel2 = await dao.getAllSubComments(res.locals.posts[i].blog_id, commentsLevel1[l].comment_id);
                res.locals.posts[i].comments[j].commentsLevel1[l].commentsLevel2 = commentsLevel2; //get level 2 comments;

                for (let k=0;k<commentsLevel2.length;k++) { //update post_date with only 18 chars
                    res.locals.posts[i].comments[j].commentsLevel1[l].commentsLevel2[k].post_date=(commentsLevel2[k].post_date.toString()).substring(4,21);
                }
            }
        }


    }
    res.render("default");
});

router.post("/default1", async function(req, res) {

    let sort = "a.post_date";
    let filter = req.body.filter;
    if (req.body.sort!=undefined){
        sort = req.body.sort;
    }
    if (filter!="") {
        filter = "'%" + filter + "%'";
    }
    let userId = "";
    let posts;
    if (req.session.user) {
            userId = req.session.user.user_id;
            res.locals.username = req.session.user.username;
        }
        posts = await dao.searchAndSortBy(sort, filter, userId);
        if (posts == "") {
            res.locals.emptyEntry = true;
        }else {
             res.locals.posts = posts;
        }
        res.cookie("posts", posts);

    //res.render("/default");
    res.send(posts);
});


router.post("/insertComment", async function(req, res) {
    let comment = await dao.insertComment(req.body.blog_id, req.body.user_id, req.body.parent_comment_id, req.body.commentInput);

    res.send(comment);
});

router.post("/updateScore", async function(req, res) {
    console.log("route receive blog_id " + req.body.blog_id);
    let  score = await dao.queryScoreByBlogId(req.body.blog_id);

    res.send(score);
});


    router.get("/home", async function (req, res) {
    res.locals.loggedIn= true;
    res.locals.title = "home";
        let posts;
        let comments;
        if (req.session.user) {
            const userId = req.session.user.user_id;
            res.locals.username = req.session.user.username;
            res.locals.loggedIn = true;

            res.locals.user_id = req.session.user.user_id;

            if (req.cookies.posts==undefined){
                const posts = await dao.searchAndSortBy("post_date", "", userId);
                res.locals.posts = posts;
            }else {
                res.locals.posts =  req.cookies.posts;
            }
            for (let i=0;i<res.locals.posts.length;i++){
                // res.locals.posts.length  loop through res.locals.posts, for every of them:
                let comments=[];
                let commentsNull=[];
                let commentsLevel1=[];
                let commentsLevel2=[];
                comments = await dao.getAllCommentsForEachBlog(res.locals.posts[i].blog_id); //get all comments from db

                for (let j=0;j<comments.length;j++){ //loop through comments, for each one:
                    if (comments[j].parent_comment_id==null){//if parent_id==null that means it's level 1 comment
                        commentsNull.push(comments[j]); }
                }

                res.locals.posts[i].comments=commentsNull;
                res.locals.posts[i].commentsNum = comments.length;

                // console.log("i blog_id comments " + i + " " + res.locals.posts[i].blog_id + " " + res.locals.posts[i].comments);
                //get level 0 comments;
                for (let k=0;k<commentsNull.length;k++) { //update post_date with only 18 chars
                    res.locals.posts[i].comments[k].post_date=(commentsNull[k].post_date.toString()).substring(4,21);
                }

                for (let j=0;j<commentsNull.length;j++) {
                    commentsLevel1 = await dao.getAllSubComments(res.locals.posts[i].blog_id, commentsNull[j].comment_id);
                    res.locals.posts[i].comments[j].commentsLevel1 = commentsLevel1; //get level 1 comments;

                    for (let k=0;k<commentsLevel1.length;k++) { //update post_date with only 18 chars
                        res.locals.posts[i].comments[j].commentsLevel1[k].post_date=(commentsLevel1[k].post_date.toString()).substring(4,21);
                    }

                    for (let l=0;l<commentsLevel1.length;l++){
                        commentsLevel2 = await dao.getAllSubComments(res.locals.posts[i].blog_id, commentsLevel1[l].comment_id);
                        res.locals.posts[i].comments[j].commentsLevel1[l].commentsLevel2 = commentsLevel2; //get level 2 comments;

                        for (let k=0;k<commentsLevel2.length;k++) { //update post_date with only 18 chars
                            res.locals.posts[i].comments[j].commentsLevel1[l].commentsLevel2[k].post_date=(commentsLevel2[k].post_date.toString()).substring(4,21);
                        }
                    }
                }


            }
            res.render("home");
        } else {
            res.redirect("./login?message=Please log in!");
        }
});

module.exports = router;
