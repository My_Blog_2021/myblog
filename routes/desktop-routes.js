// desktop routes

const express = require("express");
const router = express.Router();
const userDao = require("../modules/users-dao-zixuan.js");
const { v4: uuidv4 } = require('uuid');
const bcrypt = require("bcrypt");

//
router.post("/desktop/login", async function(req, res) {
    // Get the username and password submitted in the form
    const username = req.body.username;
    const password = req.body.password;

    // Find a matching user in the database
    const user = await userDao.retrieveUserByUsername(username);
    console.log(user);
    if (user === undefined) {
        res.json({
            status: 401,
            token: null
        })

    } else {
        // check if there is a matching password
        const isMatch = bcrypt.compareSync(password, user.encrypt_pwd);
        if (isMatch) {
            const token = uuidv4();
            // insert token to the database
            const perId = user.user_id;
            await userDao.createToken(token, perId);
            res.json({
                status: 204,
                token: token
            })
        } else {
            res.json({
                status: 401,
                token: null
            })
        }
    }


});

router.get("/users", async function(req, res) {
    const token = req.query.token;

    // Find a matching user in the database
    const user = await userDao.retrieveUserByToken(token);

    // check the permission
    if (user === undefined) {
        res.json({
            status: 401
        })
    } else {
        const permission = user.permission_level;

        if (permission === 1) {
            const allUsersInfo = await userDao.retrieveAllUsersInfo();
            res.json({
                status: 204,
                allUsersInfo: allUsersInfo
            })
        } else {
            res.json({
                status: 401
            })
        }
    }

});

router.get("/desktop/logout", async function(req, res) {
    const token1 = req.query.token;
    const token2 = null;

    // delete matching token in the database
    await userDao.deleteToken(token2, token1);

    res.json({
        status: 204
    })

});

router.delete("/users/delete", async function(req, res) {

    const userId = req.body.userId;
    const token = req.body.token;

    // Find a matching user in the database
    const user = await userDao.retrieveUserByToken(token);

    console.log(user);
    // check the permission
    if (user === undefined) {
        res.json({
            status: 401
        })
    } else {
        const permission = user.permission_level;

        if (permission === 1) {
            await userDao.deleteUserById(userId);
            res.json({
                status: 204,
            })
        } else {
            res.json({
                status: 401
            })
        }
    }

});

module.exports = router;
