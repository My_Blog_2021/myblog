//This file is only a template. Please modify this file as required.

// Setup Express
const express = require("express");
const app = express();
const port = 3000;

// Setup Handlebars
const handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({
    defaultLayout: "main"
}));
app.set("view engine", "handlebars");

// Setup bcrypt
const bcrypt = require("bcrypt");

// Setup fs
const fs = require("fs");

// Setup jimp
const jimp = require("jimp");

// Setup body-parser
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(bodyParser.text());


// Setup cookie-parser
const cookieParser = require("cookie-parser");
app.use(cookieParser());

// Setup multer (files will temporarily be saved in the "temp" folder).
const path = require("path");
const multer = require("multer");
const upload = multer({
    dest: path.join(__dirname, "temp")
});


// Setup express-session
const session = require("express-session");
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "CS719"
}));

// Make the "public" folder available statically
app.use(express.static(path.join(__dirname, "public")));


// // Setup our routes
//const loginRouter = require("./routes/login-routes.js");
// app.use(loginRouter);

// Setup our routes zixuan
const loginRouter = require("./routes/login-routes.js");
app.use(loginRouter);

const createAccountRouter = require("./routes/create-account-routes.js");
app.use(createAccountRouter);

const editDeleteRouter = require("./routes/edit-delete-routes.js");
app.use(editDeleteRouter);

const logoutRouter = require("./routes/logout-routes.js");
app.use(logoutRouter);

const likeDislikeScore = require("./routes/posts-like-dislike-routes.js");
app.use(likeDislikeScore);

const tag = require("./routes/tag-routes.js");
app.use(tag);

const deleteAndHide = require("./routes/comment-delete-author-hide-routes.js");
app.use(deleteAndHide);

const post = require("./routes/posts-routes.js");
app.use(post);

const uploadFile = require("./routes/upload-routes.js");
app.use(uploadFile);

const editAndDeletePosts = require("./routes/posts-edit-delete-routes.js");
app.use(editAndDeletePosts);

const desktop = require("./routes/desktop-routes.js");
app.use(desktop);

//const appRouter = require("./routes/application-routes.js");
//app.use(appRouter);


// Start the server running. Once the server is running, the given function will be called, which will
// log a simple message to the server console. Any console.log() statements in your node.js code
// can be seen in the terminal window used to run the server.
app.listen(port, function() {
    console.log(`Example app listening on port ${port}!`);
});
