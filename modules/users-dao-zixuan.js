const database = require("./database.js");

/**
 * Inserts the given user into the database. Then, reads the ID which the database auto-assigned, and adds it
 * to the user.
 *
 * @param user the user to insert
 */
async function createUser(user) {
    const db = await database;

    const result = await db.query(
        `insert into account (username, fname, lname, birthday, description, encrypt_pwd, avatar) 
            values(?, ?, ?, ?, ?, ?, ?)`,
            [user.username, user.firstName, user.lastName, user.birthDate, user.description, user.password, user.avatar]);

    user.id = result.insertId;
}

/**
 * Gets the user with the given username from the database.
 * If there is no such user, undefined will be returned.
 *
 * @param {string} username the id of the user to get.
 */
async function retrieveUserByUsername(username) {
    const db = await database;

    const user = await db.query(
        "select * from account where username = ?",
        [username]);

    return user[0];
}

/**
 * Gets the user with the given id from the database.
 * If there is no such user, undefined will be returned.
 *
 * @param {number} id the id of the user to get.
 */
async function retrieveUserByID(id) {
    const db = await database;

    const user = await db.query(
        "select * from account where user_id = ?",
        [id]);

    return user[0];
}

/**
 * Gets all users.
 */
async function retrieveAllUsername() {
    const db = await database;

    const allUser = await db.query(
        "select * from account ");

    return allUser;
}

/**
 * Gets the user with the given username and password from the database.
 * If there is no such user, undefined will be returned.
 *
 * @param {string} username the user's username
 * @param {string} password the user's password
 */
async function retrieveUserWithCredentials(username, password) {
    const db = await database;

    const user = await db.query(
        "select * from account where username = ? and encrypt_pwd = ?",
        [username, password]);

    return user[0];
}

/**
 * Delete the user with the given username from the database.
 * @param {number} id the user's id
 */
async function deleteUserById(id) {
    const db = await database;

     await db.query(
        "delete from account where user_id = ?",
        [id]);
}

/**
 * Updates the given user in the database.
 *
 * @param user the user to update
 */
async function updateUser(user) {
    const db = await database;

    await db.query(
        "update account set username = ?, fname = ?, lname = ?, birthday = ?, description = ?, avatar = ? where user_id = ?",
        [user.username, user.firstName, user.lastName, user.birthDate, user.description, user.avatar, user.user_id]);
}

/**
 * Updates the given user in the database.
 *
 * @param {string} password password to update
 * @param {number} user_id the user's id
 */
async function updateUserPassword(password,user_id) {
    const db = await database;

    await db.query(
        "update account set encrypt_pwd = ? where user_id = ?",
        [password, user_id]);
}

/**
 * Gets the blog with the given id from the database.
 * If there is no such blog, undefined will be returned.
 *
 * @param {number} blog_id the id of the user to get.
 */
async function retrieveBlogByBlogID(blog_id) {
    const db = await database;

    const blog = await db.query(
        "select * from blogs where blog_id = ?",
        [blog_id]);

    return blog[0];
}
/**
 * Updates the given post in the database.
 *
 * @param post the post to update
 */
async function updatePostContent(post) {
    const db = await database;

    await db.query(
        "update blogs set post_title = ?, post_tag1 = ?, post_tag2 = ?, post_tag3 = ?, post_content = ? where blog_id = ?",
        [post.post_title, post.post_tag1, post.post_tag2, post.post_tag3, post.post_content, post.blog_id]);
}

/**
 * Updates the given post in the database.
 *
 * @param post the post to update
 */
async function updatePostImage(post) {
    const db = await database;

    await db.query(
        "update blogs set post_title = ?, post_tag1 = ?, post_tag2 = ?, post_tag3 = ?, post_image = ? where blog_id = ?",
        [post.post_title, post.post_tag1, post.post_tag2, post.post_tag3, post.post_image, post.blog_id]);
}

/**
 * Updates the given post in the database.
 *
 * @param post the post to update
 */
async function updatePostVideo(post) {
    const db = await database;

    await db.query(
        "update blogs set post_title = ?, post_tag1 = ?, post_tag2 = ?, post_tag3 = ?, post_video = ? where blog_id = ?",
        [post.post_title, post.post_tag1, post.post_tag2, post.post_tag3, post.post_video, post.blog_id]);
}

/**
 * Updates the blogs' status in the database.
 *
 * @param {boolean} isEdited
 * @param {number} blog_id the blog's id
 */
async function updateBlogStatus(isEdited,blog_id) {
    const db = await database;

    await db.query(
        "update blogs set edit = ? where blog_id = ?",
        [isEdited, blog_id]);
}


/**
 * Updates the given post in the database.
 *
 * @param {number} blog_id the post to delete
 */
async function deletePostById(blog_id) {
    const db = await database;

    await db.query(
        "delete from blogs where blog_id = ?",
        [blog_id]);
}

/**
 * Gets the comment with the given id from the database.
 * If there is no such blog, undefined will be returned.
 *
 * @param {number} comment_id the id of the comment to get.
 */
async function retrieveCommentByCommentID(comment_id) {
    const db = await database;

    const comment = await db.query(
        "select * from comment where comment_id = ?",
        [comment_id]);

    return comment[0];
}

/**
 * Updates the comments' status in the database.
 *
 * @param {boolean} isVisible
 * @param {number} comment_id the comment's id
 */
async function updateCommentStatus(isVisible,comment_id) {
    const db = await database;

    await db.query(
        "update comment set visible = ? where comment_id = ?",
        [isVisible, comment_id]);
}

/**
 * Delete the user with the given username from the database.
 * @param {number} comment_id the comment's id
 */
async function deleteCommentById(comment_id) {
    const db = await database;

    await db.query(
        "delete from comment where comment_id = ?",
        [comment_id]);
}

/**
 * update the content in the database.
 * @param {string} comment_content the comment's content
 * @param {number} comment_id the comment's id
 */
async function updateCommentContentById(comment_content, comment_id) {
    const db = await database;

    await db.query(
        "update comment set comment = ? where comment_id = ?",
        [comment_content, comment_id]);
}
/**
 *Updates the blog's score with the given id.
 *
 * @param {number} blog_id the id of the blog to get.
 * @param {number} post_score the score of the post to update.
 */
async function updateScore(blog_id, post_score) {
    const db = await database;

    await db.query(
        "update blogs set post_score = ? where blog_id = ?",
        [post_score, blog_id]);

}

/**
 * Inserts the tag into the database. .
 *
 * @param tag_content the tag's content to insert
 */
async function createTag(tag_content) {
    const db = await database;

    const result = await db.query(
        `insert into tags (tag_content) 
            values(?)`,
        [tag_content]);

}


/**
 * Gets tags grouped by tag_content sorted by count.
 */
async function retrieveTagsSorted() {
    const db = await database;

    const sortedTags = await db.query(
        "select tag_id, tag_content, count(tag_content) from tags group by tag_content order by count(tag_content) desc");

    return sortedTags;
}

/**
 * Gets sorted tags by tag_content.
 * @param tag_content the tag's content to get
 */
async function retrieveFilteredTagsByContent(tag_content) {
    const db = await database;

    const filteredTags = await db.query(
        "select *  from (select tag_id, tag_content, count(tag_content) from tags group by tag_content order by count(tag_content) desc) as temp where tag_content like (?)",
        [tag_content]);

    return filteredTags;
}

/**
 * Gets the permission with the given id from the database.
 *
 * @param {string} token the id of the user to get.
 */
async function retrieveUserByToken(token) {
    const db = await database;

    const user = await db.query(
        "select * from permission where token = ?",
        [token]);

    return user[0];
}

/**
 * Inserts the token into the database. .
 *
 * @param token the user's token to insert
 * @param per_id the user's id to insert
 */
async function createToken(token, per_id) {
    const db = await database;

    const result = await db.query(
        "update permission set token = ? where per_id = ?",
        [token, per_id]);
}

/**
 * delete the token into the database. .
 *
 * @param token2 null
 * @param token1 the user's token
 */
async function deleteToken(token2, token1) {
    const db = await database;

    const result = await db.query(
        "update permission set token = ? where token = ?",
        [token2, token1]);
}

/**
 * Gets all users' token.
 */
async function retrieveAllToken() {
    const db = await database;

    const allTokens = await db.query(
        "select * from permission ");

    return allTokens;
}

/**
 * Gets all users' profile information.
 */
async function retrieveAllUsersInfo() {
    const db = await database;

    const allUsersInfo = await db.query(
        "select * from account ");

    return allUsersInfo;
}

// Export functions.
module.exports = {
    createUser,
    retrieveUserByUsername,
    retrieveUserByID,
    retrieveAllUsername,
    retrieveUserWithCredentials,
    deleteUserById,
    updateUser,
    updateUserPassword,
    retrieveBlogByBlogID,
    updatePostContent,
    updatePostImage,
    updatePostVideo,
    updateBlogStatus,
    deletePostById,
    retrieveCommentByCommentID,
    updateCommentStatus,
    deleteCommentById,
    updateCommentContentById,
    updateScore,
    createTag,
    retrieveTagsSorted,
    retrieveFilteredTagsByContent,
    retrieveUserByToken,
    createToken,
    deleteToken,
    retrieveAllToken,
    retrieveAllUsersInfo


};

