const  database = require("./database.js");

/**
 * Inserts the post into the database. Then, reads the ID which the database auto-assigned, and adds it
 * to the post.
 *
 * @param post the post to publish
 */
async function  createPost(post) {
    const db = await database;
    const result = await db.query(
        `insert into blogs (user_id, post_title, post_tag1, post_tag2, post_tag3, post_score, post_content)
            values(?, ?, ?, ?, ?, ?, ?)`,
        [post.author,post.title, post.tag1, post.tag2, post.tag3, post.score, post.content]);
    post.id = result.insertId;
}

async function uploadPic(picture) {
    const db = await database;
    const result = await db.query(
        `insert into blogs (user_id, post_title, post_tag1, post_tag2, post_tag3, post_score, post_image)
        values(?, ?, ?, ?, ?, ?, ?)`,
        [picture.author, picture.title, picture.tag1, picture.tag2, picture.tag3, picture.score, picture.image]);
}

async function uploadVideo(video) {
    const db = await database;
    const result = await db.query(
        `insert into blogs (user_id, post_title, post_tag1, post_tag2, post_tag3, post_score, post_video)
        values(?, ?, ?, ?, ?, ?, ?)`,
        [video.author, video.title, video.tag1, video.tag2, video.tag3, video.score, video.video]);
}
/**
 * Gets all posts of the given username from the database.
 * If there is no such user, undefined will be returned.
 *
 * @param {string} username the id of the user to get.
 */
async function retrievePostByUserid(user_id) {
    const db = await database;

    const post = await db.query(
        "select * from blogs where user_id = ?",
        [user_id]);

    return post;
}

/**
 * Delete the post with the given postid from the database.
 * @param {string} psotid the post's postid
 */
async function deletePostByPostid(postid) {
    const db = await database;

    await db.query(
        "delete from blogs where post_id = ?",
        [postid]);
}

/**
 * Updates the given post in the database.
 *
 * @param post the post to update
 */
async function updatePost(post) {
    const db = await database;

    await db.query(
        "update blogs set user_id = ?, user_icon = ?, post_title = ?, post_content = ?, where blog_id = ?",
       [post.author, post.avatar, post.title, post.content]);
}

module.exports = {
    createPost,
    retrievePostByUserid,
    deletePostByPostid,
    updatePost,
    uploadPic,
    uploadVideo
};
