const database = require("./database.js");

async function searchAndSortBy(sort, filter, userID) {
    const db = await database;
    let posts;
    if (sort=="username"){ sort = sort + " ASC"; } else { sort = sort + " DESC"; }

    if (userID==""&&filter=="") {
            posts = await db.query("SELECT a.blog_id, a.user_id, a.post_title, a.post_tag1, a.post_tag2, a.post_tag3, a.post_score, a.post_date, a.post_image, a.post_video, a.post_content, a.edit, b.username as username, b.avatar as avatar FROM blogs as a, account as b WHERE a.user_id=b.user_id ORDER BY "+sort+"");
    }else if (userID==""&&filter!=""){
            posts = await db.query(" SELECT a.blog_id, a.user_id, a.post_title, a.post_tag1, a.post_tag2, a.post_tag3, a.post_score, a.post_date, a.post_image, a.post_video, a.post_content, a.edit, b.username as username, b.avatar as avatar FROM blogs as a, account as b WHERE a.user_id=b.user_id AND (a.post_title like  "+filter+" OR a.post_content like "+filter+") ORDER BY "+sort+"");
    }else if (userID!=""&&filter=="") {
                posts = await db.query("SELECT a.blog_id, a.user_id, a.post_title, a.post_tag1, a.post_tag2, a.post_tag3, a.post_score, a.post_date, a.post_image, a.post_video, a.post_content, a.edit, b.username as username, b.avatar as avatar FROM blogs as a, account as b WHERE a.user_id=b.user_id AND a.user_id="+userID+" ORDER BY "+sort+"");
    }else if (userID!=""&&filter!="") {
        posts = await db.query("select a.comment_id,a.blog_id, a.user_id, a.post_date, a.parent_comment_id, a.comment, b.username, b.avatar, b.fname from comment as a, account as b, blogs as c where a.blog_id=c.blog_id and c.user_id=b.user_id and a.blog_id=1  ORDER BY  a.parent_comment_id ASC, a.post_date DESC");
    }
    return posts;
}

async function getAllCommentsForEachBlog(blogId) {
    const db = await database;
    const comments = await db.query( "select a.comment_id,a.blog_id, a.user_id, a.post_date, a.parent_comment_id, a.comment, b.username, b.avatar, b.fname  from comment as a, account as b, blogs as c where a.blog_id=c.blog_id and b.user_id=a.user_id and a.blog_id= "+blogId+" ORDER BY a.post_date DESC, a.parent_comment_id ASC");
    return comments;
}


async function getAllCommentsForEachBlog(blogId) {

    const db = await database;
    const comments = await db.query( "select a.visible, a.comment_id,a.blog_id, a.user_id, a.post_date, a.parent_comment_id, a.comment, b.username, b.avatar, b.fname  from comment as a, account as b, blogs as c where a.blog_id=c.blog_id and b.user_id=a.user_id and a.blog_id= "+blogId+" ORDER BY a.post_date DESC, a.parent_comment_id ASC");

    return comments;

}


async function getAllSubComments(blogId, commentId) {

    const db = await database;
    const comments = await db.query( "select a.visible, a.comment_id,a.blog_id, a.user_id, a.post_date, a.parent_comment_id, a.comment, b.username, b.avatar, b.fname  from comment as a, account as b, blogs as c where a.blog_id=c.blog_id and b.user_id=a.user_id and a.blog_id= "+blogId+" and a.parent_comment_id="+commentId+"  ORDER BY a.post_date DESC, a.parent_comment_id ASC");

    return comments;
}

async function insertComment(blog_id, user_id, parent_comment_id, commentInput) {
    const db = await database;
    let result;
    if (parent_comment_id==""){
        parent_comment_id = "NULL";
    }
    commentInput="'" + commentInput + "'";
    result = await db.query("insert into comment (blog_id, user_id, parent_comment_id,comment, visible) VALUES ("+blog_id+", "+user_id+", "+parent_comment_id+", "+commentInput+", true)");
    return result;
}

async function queryScoreByBlogId(blog_id){

    const db = await database;
    const score = await db.query( "select post_score from blogs where blog_id= "+blog_id+"");

    return score[0];
}

module.exports = {
    searchAndSortBy,
    getAllCommentsForEachBlog,
    getAllSubComments,
    insertComment,
    queryScoreByBlogId
};
