// commenter delete and author hide

window.addEventListener("load", function() {

    // author can hide comments
    // check if is author
    const hideArr = document.querySelectorAll(".authorHide");
    const commentTextArr = document.querySelectorAll(".commentText")
    const commentContainerArr = document.querySelectorAll(".commentContainer");

    // check all hidden comments and hide
    for (let i = 0; i < hideArr.length; i++) {

        hideArr[i].onclick = hideComment;

        function hideComment() {
            // get blogId and commentId and content
            const blogId = hideArr[i].getAttribute("blogId");
            const commentId = hideArr[i].getAttribute("commentId");
            const visible = hideArr[i].getAttribute("visible");

            console.log(visible);
            // check is author
            $.ajax({
                type: "POST",
                url: "/default/isAuthor",
                data:{
                    blog_id: blogId,
                },
                success: function(data){

                    if (data == "1") {
                        hideOrDisplay();
                    } else {
                        alert("You cannot do it!");
                    }
                }})

            // hide or display
            function hideOrDisplay() {
                // display
                if (visible === "0") {
                    hideArr[i].setAttribute("src", "../assets/icon/hide.png");
                    $.ajax({
                        type: "POST",
                        url: "/default/hide",
                        data:{
                            comment_id: commentId,
                            visible: true
                        },
                        success: function(data){
                            commentTextArr[i].innerHTML = data;
                            hideArr[i].setAttribute("visible","1");
                        }})

                } else {
                    //hide
                    hideArr[i].setAttribute("visible","0");
                    hideArr[i].setAttribute("src", "../assets/icon/display.png");
                    commentTextArr[i].innerHTML = "-- Hidden by post author! --";
                    $.ajax({
                        type: "POST",
                        url: "/default/hide",
                        data:{
                            comment_id: commentId,
                            visible: false
                        }})
                }


            }

        }
    }

    // commenter can delete
    const deleteArr = document.querySelectorAll(".commenterDelete");
    for (let i = 0; i < deleteArr.length; i++) {
        deleteArr[i].onclick = deleteComment;

        function deleteComment() {
            const commentId = deleteArr[i].getAttribute("commentId");
            const blogId = deleteArr[i].getAttribute("blogId");
            // check if is commenter
            $.ajax({
                type: "POST",
                url: "/default/isCommenter",
                data:{
                    comment_id: commentId
                },
                success: function(data){
                    console.log(data);
                    if (data == "1") {
                        checkHasReply(commentId);
                    } else {
                        alert("You cannot do it!");
                    }
                }})
        }

        // check if it has reply
        function checkHasReply(commentId) {
            let hasReply = false;
            for (let j = 0; j < deleteArr.length; j++) {
                // has reply
                if (deleteArr[j].getAttribute("parentId") === commentId) {
                    hasReply = true;
                    commentTextArr[i].innerHTML = "-- Deleted by comment author! --";
                    console.log(j);

                }
            }
            // no reply
            console.log(hasReply);
            if (hasReply === false) {
                commentContainerArr[i].style.display = "none";
                console.log("all")
            }

            $.ajax({
                type: "POST",
                url: "/default/delete",
                data:{
                    comment_id: commentId,
                    hasReply: hasReply
                }})

        }


    }







});
