
window.addEventListener("load", function() {

    deleteCookie("posts");
    let radioBtns = document.querySelector(".radio-btn");
    let userIcons = document.querySelectorAll(".icon-image");
    let postTitles = document.querySelectorAll(".card-title");
    let usernames = document.querySelectorAll(".user-name");
    let postContents = document.querySelectorAll(".post-content");
    let cardImgs = document.querySelectorAll(".card-image");
    let postScore = document.querySelectorAll(".post-score");
    let postId = document.querySelectorAll(".post-id");
    let goBtn = document.querySelector(".btn-light");
    let searchText = document.querySelector(".text-xl-right");
    let blogs = document.querySelectorAll(".card");
    let commentForEachBlog =  document.querySelectorAll(".comment-table");
    let allCommentsBtns = document.querySelectorAll("#allCommentsBtn");
    let allComments = document.querySelectorAll(".allComments");
    let commentNumber =  document.querySelectorAll(".comment-number");

    let commentTextAreasDivs = document.querySelectorAll( ".commentTextAreasDivs");
    let commentTextAreas = document.querySelectorAll( ".commentTextAreas");
    let createCommentBtns = document.querySelectorAll(".createCommentBtns");
    let submitCommentBtns =  document.querySelectorAll( ".submitCommentBtns");

   let timerReference = setInterval(changeScoreDisplay, 60000);

   function changeScoreDisplay() {
      let blog_id = [];
       for (let i = 0; i < postScore.length; i++) {
           blog_id.push(postScore[i].getAttribute("blogId"));
         }

       let Result
       for (let i = 0; i < blog_id.length; i++) {
           Result = updateScore(blog_id[i]);
          postScore[i].innerHTML = "Score: " + Result.post_score;
       }

       function updateScore(blog_id) {
           var result = '';
           $.ajax({
               async: false,
               type: "POST",
               url: "/updateScore",
               data: {blog_id: blog_id},
               dataType: "json",
               success: function (callback) {
                   result = callback;
               },
               error: function (jqXHR) {
                   alert("error happens: " + jqXHR.status);
               }
           });
           return result;
       }
   }

    for (let i=0;i<createCommentBtns.length;i++) {
        createCommentBtns[i].addEventListener("click", () => {
            commentTextAreasDivs[i].style.display = "block";

        });
    }

    for (let i=0;i<submitCommentBtns.length;i++) {
        submitCommentBtns[i].addEventListener("click", () => {
            let commentInput = commentTextAreas[i].value;
            let blog_id = createCommentBtns[i].getAttribute("blog_id");
            let user_id = document.querySelector(".h1").getAttribute("user_id");
            let parent_comment_id = createCommentBtns[i].getAttribute("parent_comment_id");
            let visible = createCommentBtns[i].getAttribute("visible");


            let result = insertComment (blog_id, user_id,parent_comment_id, commentInput );

        //    alert("i "+ i + " classes " + createCommentBtns[i].classList + "blog_id=  " + blog_id +" user_id= " + user_id + " parent_comment_id= " + parent_comment_id+ " commentInput= "+ commentInput);

            if (result) {

                submitCommentBtns[i].innerHTML = "Submitted successfully";
                commentTextAreas[i].setAttribute("disabled","true");
                commentTextAreas[i].disabled=true;
                submitCommentBtns[i].disabled=true;
                createCommentBtns[i].disabled = true;
                confirm("Please refresh page to review your comment.");
            }


        });
    }

    let startNums = [];
    let startNum=0;
    startNums.push(0);
    for (let i=0;i<commentNumber.length;i++){
        startNum=startNum+parseInt(commentNumber[i].innerHTML);
        startNums.push(startNum);
    }

    setOriginalCommentDisplay();

    for (let i=0;i<allCommentsBtns.length;i++) {
        allCommentsBtns[i].addEventListener("click", () => {
        let displayNum = 0;
        let btnStatus = allCommentsBtns[i].innerHTML;
        if (btnStatus == "Collapse"){
            setOriginalCommentDisplay();
            allCommentsBtns[i].innerHTML = "View more comments...";
        }
        else {
            for (let j = startNums[i]; j < startNums[i + 1]; j++) {
                if ($(allComments[j]).is(':visible')) {
                    displayNum = j;
                }
            }
            for (let k = (displayNum + 1); k < startNums[i + 1]; k++) {
                allComments[k].style.display = "flex";
            }
            if ($(allComments[startNums[i + 1] - 1]).is(':visible')) {
               allCommentsBtns[i].innerHTML = "Collapse";
            }
        }
        });
    }

    $('input[type=radio][name=sortOptions]').change(function () {
        let sortField = $("input[name=sortOptions]:checked").val();
        deleteCookie("posts");
        let sortResult = queryDb(sortField,"");

        updatePageWithoutReload(sortResult);

    });

    goBtn.addEventListener("click", () => {
        let sortField = $("input[name=sortOptions]:checked").val();
        let filterText = searchText.value;
        deleteCookie("posts");
        let queryResult = queryDb(sortField, filterText);
            if(searchText.value==""){
                if(sortField != undefined){
                    updatePageWithoutReload(queryResult);
                }else {
                    top.location.reload();
                }
            }else {
           top.location.reload();
            }
    });




    function queryDb(sort, filter) {

        var result = '';
        $.ajax({
            async: false,
            type: "POST",
            url: "/default1",
            data: {sort: sort,filter: filter},
            dataType: "json",
            success: function (callback) {
                result = callback;
            },
            error: function (jqXHR) {
                alert("error happens: " + jqXHR.status);
            }
        });
        return result;
    }


    function updatePageWithoutReload(sortResult) {

        for (let i = 0; i < sortResult.length; i++) {
            userIcons[i].src = "/avatar" + sortResult[i].avatar;
            postTitles[i].innerHTML = sortResult[i].post_title;
            usernames[i].innerHTML = "@" + sortResult[i].username;
            postScore[i].innerHTML = "Score: " + sortResult[i].post_score;
            postId[i].innerHTML = "Post ID: " + sortResult[i].blog_id;
            postContents[i].innerHTML = sortResult[i].post_content;
            cardImgs[i].src = "/avatar" + sortResult[i].post_file + ".jpg";
        }
    }


    function removeExtraCards(queryResult) {
        for (let j=queryResult.length;j<cards.length;j++){
            cards[j].remove();
        }
    }
    async function fetchPostsa(queryResult) {
        const response = await fetch(`default2?id=${queryResult}`);
    }

    function deleteCookie(cname) {
        const d = new Date();
        d.setDate(0);
        document.cookie = `${cname}= ; expires=${d.toUTCString()}; path=/`;
    }

    function setOriginalCommentDisplay (){

        for (let i=0; i<blogs.length; i++ ) {
            for (let j = startNums[i]; j < startNums[i+1]; j++) {
                if ((j < (startNums[i]+5))) {
                    allComments[j].style.display = "flex";
                } else {
                    allComments[j].style.display = "none";
                }
            }
        }
    }


    function insertComment (blog_id, user_id,parent_comment_id, commentInput ) {

        var result = '';
        $.ajax({
            async: false,
            type: "POST",
            url: "/insertComment",
            data: { commentInput: commentInput,
                blog_id: blog_id,
                user_id: user_id,
                parent_comment_id: parent_comment_id},
            dataType: "json",
            success: function (callback) {
                result = callback;
            },
            error: function (jqXHR) {
                alert("error happens: " + jqXHR.status);
            }
        });
        return result;
    }


});
