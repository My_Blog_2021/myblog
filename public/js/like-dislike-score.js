// add like and dislike click event
window.addEventListener("load", function() {

    // check if logged in
    $.ajax({
        type: "POST",
        url: "/posts/isLogin",
        success: function(data){
            if (data == "1") {
                likeAndDislike();
                document.querySelector(".alert").style.display = "none";
            } else {
                document.querySelector(".alert").innerHTML = "You cannot like/dislike or create/delete/hide comments without logging in! Please log in!";
                //Yan: disable button.

                let createCommentBtns = document.querySelectorAll(".createCommentBtns");
                let submitCommentBtns =  document.querySelectorAll( ".submitCommentBtns");
                for (let i=0; i<createCommentBtns.length;i++){
                    createCommentBtns[i].disabled = true;
                    submitCommentBtns[i].disabled = true;
                }

            }
        }})

    // score changed when user like or dislike
    function likeAndDislike() {

    let blogScore = null;
    let blogId = 0;
    let score = 0;

    const blogScoreArr = document.querySelectorAll(".post-score");
    const scoreArr = document.querySelectorAll(".score");

    for (let i = 0; i < scoreArr.length; i++) {
        scoreArr[i].onclick = postScore;

        function postScore(){
            blogId = scoreArr[i].getAttribute("blogId");

            // query which position should be displayed score by blogId
            for (let j = 0; j < blogScoreArr.length; j++) {
                if (blogScoreArr[j].getAttribute("blogId") === blogId) {
                    blogScore = blogScoreArr[j];
                }
            }

                 // check like or dislike
                if (scoreArr[i].classList.contains("like")) {
                    scoreArr[i].setAttribute("src","../assets/icon/likeIcon2.png");
                    scoreArr[i+1].onclick = null;
                    score = 1;
                } else {
                    scoreArr[i].setAttribute("src","../assets/icon/dislikeIcon2.png");
                    scoreArr[i-1].onclick = null;
                    score = -1;
                }
            scoreArr[i].onclick = null;

            getScoreByAjax(blogId, score);

        }
    }


    function getScoreByAjax(blogId, score) {
        $.ajax({
            type: "POST",
            url: "/posts/score",
            data: {
                blog_id: blogId,
                score: score},
            dataType: "json",
            success: function(data){
                blogScore.innerHTML = data;
            }})
    }
    }
});
