// add tag's onchange event
window.addEventListener("load", function() {
    const tagArr = document.querySelectorAll(".tag");
    const tagInputArr = document.querySelectorAll(".tagInput");
    const ul = document.createElement("ul");

    ul.style.padding = 0;
    ul.style.listStyle = "none";

    let tagValue;
    let filteredTag = [];

    // the existed tags list will be displayed when user type
    for (let i = 0; i < tagArr.length; i++) {
        tagArr[i].onclick = function () {
            ul.innerHTML = "";
            tagArr[i].onkeyup = function () {
                ul.innerHTML = "";
                tagInputArr[i].appendChild(ul);
                tagValue = tagArr[i].value;
                $.ajax({
                    type: "POST",
                    url: "/posts/filter",
                    data: {
                        tagContent: tagValue
                    },
                    dataType: "json",
                    success: function (data) {

                        filteredTag = data;
                        for (let j = 0; j < filteredTag.length; j++) {
                            const li = document.createElement("li");
                            ul.appendChild(li);
                            li.innerText = filteredTag[j];
                        }

                        // put the clicked value in the input
                        const li = document.querySelectorAll("li");
                        for (let j = 0; j < li.length; j++) {
                            li[j].onclick = function () {
                                tagValue = li[j].innerText;
                                tagArr[i].value = tagValue;
                                ul.innerHTML = "";
                            }
                        }
                    }
                });

            }

        }

    }


});

