// edit-delete

window.addEventListener("load", function() {

    const deleteArr = document.querySelectorAll(".postDelete");
    const cardArr = document.querySelectorAll(".card");
    for (let i = 0; i < deleteArr.length; i++) {
        deleteArr[i].onclick = deletePost;

        // delete event
        function deletePost() {
            const blogId = deleteArr[i].getAttribute("blogId");
            $.ajax({
                type: "POST",
                url: "/deletePost",
                data: {blogId: blogId},
                success: function(data){
                    if (data == "1") {

                        for(let j = 0; j < cardArr.length; j++) {
                            if(cardArr[j].getAttribute("blogId") === blogId) {
                                cardArr[j].innerHTML = "";
                            }
                        }
                    }
                }})
        }


    }

/*
// edit event(assign the previous post to the preVersion div)
    const editArr = document.querySelectorAll(".edit");
    const titleArr = document.querySelectorAll(".card-title");
    const contentArr = document.querySelectorAll(".postContainer");
    const tagArr = document.querySelectorAll(".tag");

    const preVersionTitleArr = document.querySelectorAll(".preVersionTitle");
    const preVersionContentArr = document.querySelectorAll(".preVersionContent");
    const preVersionTagArr = document.querySelectorAll(".preVersionTag");

    for (let i = 0; i < editArr.length; i++) {
        editArr[i].onclick = assignToPrevious;

        function assignToPrevious() {
            // get value
            const title = titleArr[i].innerText;
            const content = contentArr[i].innerHTML;
            const tag = tagArr[i].innerHTML;

            // assign
            preVersionTitleArr[i].innerText = title;
            preVersionContentArr[i].innerHTML = content;
            preVersionTagArr[i].innerHTML = tag;

        }
    }

// see previous version event

    const preVersionArr = document.querySelectorAll(".preVersion");
    const preVersionContainerArr = document.querySelectorAll(".preVersionContainer");
    for (let i = 0; i < preVersionArr.length; i++) {
        preVersionArr[i].onclick = seePreviousVersion;

        function seePreviousVersion() {
            preVersionContainerArr[i].style.display = "none";
        }


    }*/


});
