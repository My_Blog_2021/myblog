# README.md 

## Server Setup
Group Name: Hyperlinks

To begin with, simply run `npm install` in the `/myBlog` path to append all the dependencies for this project, once all the node modules have been correctly installed,
run this command: `node myBlog.js` in the root directory to start the Node server. 

The Node server will host our myBlog web page at `http://localhost:3000/`, Then type this address into the address bar of your browser to visit our blog.

**_NOTE:_**The demo account are stored in demo_account.csv, you can use this demo first, and do not run the sql file.

The browser will direct you to the default page, although you can choose to browse this page as a visitor and view other people's post here,  
**We Highly Recommend** you to register our blog to enjoy the full functionality we prepared for you.
*************************

## Environment We are Using

***Developing Tools***

| Tools | Name |
| ------------ | ------------ |
| Development Tools | IDEA, WebStorm, Chrome/FireFox Browser  |
| Language | JDK1.8, JDK11.0, org.json.jar, HTML, CSS, Javascript, Handlebar|
| Database | MariaDB                                          |
| Java Project Build | Maven                                  |
| Runtime Environment | Node.js Local Machine                 |

***NodeJS Dependencies***

| Node Modules | Version |
| ------------ | ------------ |
| body-parser | 1.19.0        |
| cookie-parser | 1.4.4       |
| core-js | 2.6.12            |
| express | 4.16.1            |
| express-handlebars | 3.1.0  |
| express-session | 1.16.2    |
| uuid | 8.3.2                |
| bcrypt | 5.0.1              |
| jimp | 0.16.1               |
| mariadb | 2.3.1             |
| bcrypt | 5.0.1              |
*************************

## How to Create Your First Blog

- Create an account
    - Firstly, click the login button on the top right corner of the default page 
    - Then click the `create a new account` button to create your account, you need to type in all the information   
      we need in this form to create an account, also you can choose one cute Pokémon™ picture as your user avatar   
    - Once all your information has been filled out, you can access all the functionality such as: post a blog, like or dislike other people's post, etc.    
    
- Create your first post
    - Click the `Post` button on the top right corner of the home page, this button will redirect you to the "Create a new post" page.
    - Select a type you would like to post: text, photo, or video    
    - Add a Title for your post
    - If you select the text button, which is the default post type, you can write what you want in the textarea.
    - If you select the photo or video button, you may click the select file button to select the corresponding file in the pop-up file selector.
    - At last, You need to add at least one tag to the input box. Multiple tags also are valid.
    - Finally, hit the submit button to post your blog.

*************************

## How to Edit Your Personal Information

Sometimes, if you think your username is not what you want, or the password is too short, you could go to the MyInfo Label to edit your personal information.  
On this page, you could see all of your account details in one place, which is nice and sweet.  
At the button of the `userInfo` page, there are two light blue Hyperlinks. You can click them at your wish.
*************************

## How to Edit Your Posts

We always respect the privacy of all our users, and we are committed to protecting your data on our server,   
therefore we allow you to delete or edit any of your own posts. To be more clear, you can always visit the `posts-edit` page to recreate or just delete   
your beloved posts.
If posts were editted by authors, word "edited"  will be displayed on the top right.
*************************

## How to Edit Comment
All users could reply to anyone's post, and in reply to other people's replies.  
All authors could hide or display their comments, just click the little eye button to hide your comment or display, only the commenter can delete their comments.  
The way to delete the comment is to click the trash bin button.
*************************

## Giving credit to others
If you like someone's post, just click the thumbs up button (you can click it only once).
Once you have clicked the button, There is no pill for regret, all the thumb up/down buttons will be locked. And you are unable to click them again.
**_NOTE:_** This action can only be done in the default page.
*************************
*************************
## WebPage Preview

**Default Home Page**
![myblog_default_page.jpeg](https://i.loli.net/2021/05/31/JkVCOxD9o5enhGz.jpg)
*************************
**Create Post**
![post-page-screenshoot.jpeg](https://i.loli.net/2021/05/30/gIu2A68GF49onvf.jpg)
*************************
**Explore Page**
![explore_page.jpeg](https://i.loli.net/2021/05/31/JTGZUC2xlVshgr9.jpg)
*************************
**Edit MyInfo**
![myinfo.jpeg](https://i.loli.net/2021/05/31/KNQkFc9nvYstejx.jpg)
*************************
**Responsive Design for Mobile Users**
![responsive.jpeg](https://i.loli.net/2021/05/31/YPzvxGsgOtJHpkr.jpg)
*************************

